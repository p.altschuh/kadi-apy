# Release history

## (unreleased)

* Add `exit-not-created` flag to CLI create tools.
* Fix order in which `_meta` is invalidated.
* Add option to download only those files from record matching pattern.

## 0.6.0 (2021-02-26)

* Add option to use certificates from ca_bundle.
* Add option skip the initial request.

## 0.5.0 (2021-02-24)

* Include a config file to store the information about host and PAT.
* Added timeout config option.
* Add autocompletion.

## 0.4.1 (2021-02-02)

* Include the --version output.
* Add handling for missing schema exception.
* Unify the behavior of decorators if information is not required.
* Add --xmlhelp runner

## 0.4.0 (2021-01-22)

* Add option to hide public resources in the CLI search.
* Functions to handle miscellaneous (license search, remove item from trash).
* Add function to remove all metadata from a record.

## 0.3.3 (2021-01-14)

* Smaller cleanups.

## 0.3.2 (2021-01-14)

* Include missing `__init__.py`.

## 0.3.1 (2021-01-14)

* Include xmlhelpy import from pypi.

## 0.3.0 (2021-01-14)

* Include definition of public api.
* Integration of --xmlhelp option to all CLI tools.
* Definition of CLI classes which can be used in additions tools.
* Print more infos when using the CLI.
* Various refactoring.

## 0.2.3 (2020-10-23)

* Fix for specifying PAT and host directly in certain cli commands.

## 0.2.2 (2020-10-23)

* Raise exception instead of sys.exit(1) for cli functions.

## 0.2.1 (2020-10-05)

* Update examples.

## 0.2.0 (2020-10-02)

* Work with identifiers for items.

## 0.1.2 (2020-09-25)

* Include group roles for records and collections.
* Add option to skip ssl/tls cert verification.

## 0.1.1 (2020-09-21)

* Improved printing for updated metadata.
* Smaller improvements.

## 0.1.0 (2020-09-09)

* Most API endpoints for managing records, collections and groups, as well as
  some others, are usable directly through the library.
* Most of that functionality is also exposed via CLI by using the kadi-apy
  command.
