#!/usr/bin/env bash
# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
set -e

cd "$(dirname $(readlink -f $0))/.."

# Install or update the application.
pip3 install -e .

# Remove all old build-related directories.
rm -rf build
rm -rf dist
rm -rf kadi_apy.egg-info

# Build the wheel.
python3 setup.py bdist_wheel

tput setaf 2
echo "Wheel built successfully."
tput sgr0
