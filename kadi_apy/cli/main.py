# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import click_completion
from xmlhelpy import group

from kadi_apy.version import __version__

click_completion.init()


@group(version=__version__)
def kadi_apy():
    """The kadi-apy command line interface."""
    # pylint: disable=unused-import


from .commands.records import records
from .commands.config import config
from .commands.collections import collections
from .commands.groups import groups
from .commands.users import users
from .commands.templates import templates
from .commands.miscellaneous import miscellaneous
